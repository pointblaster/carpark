import XCTest

import carparkTests

var tests = [XCTestCaseEntry]()
tests += carparkTests.allTests()
XCTMain(tests)
