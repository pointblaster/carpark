//
//  MainEngine.swift
//  
//
//  Created by Pontus Jacobsson on 2021-03-04.
//

import Foundation

class MainEngine {
    private static let shared = MainEngine()
    
    let minimumOfSameStateInARow = 5
    
    var carIsParked = false {
        didSet {
            if oldValue != carIsParked {
                print("PARKING CHANGED: \(carIsParked)")
                
                SlackPoster.postToSlack(carIsParked ? .parkingSpotUsed : .parkingSpotEmpty)
            }
        }
    }
    
    var latestMeasurements = [Bool]()
    
    static func start() {
        shared.checkDistance()
    }
    
    private func checkDistance() {
        DistanceSensor.measureDistance { (measureResult) in
            switch measureResult {
            case .distance(let distance):
                print(distance)
                
                self.addMeasurement(parked: distance < 100)
            case .error(let error):
                print(error)
                
                if error as? HCSR04.ErrorList == HCSR04.ErrorList.echoSignalError {
                    self.addMeasurement(parked: false)
                }
            }
        }
        
        sleep(1)
        checkDistance()
    }
    
    private func addMeasurement(parked: Bool) {
        handleLEDs(parked: parked)
        
        while latestMeasurements.count >= minimumOfSameStateInARow {
            latestMeasurements.removeFirst()
        }
        
        latestMeasurements.append(parked)
        
        if latestMeasurements.allEqual() && latestMeasurements.count >= minimumOfSameStateInARow {
            carIsParked = latestMeasurements.first ?? carIsParked
        }
    }
    
    private func handleLEDs(parked: Bool) {
        if parked == carIsParked {
            LEDFlasher.shared.setLEDValue(0)
            
            return
        }
        
        var numberOfValues = 1
        
        for value in latestMeasurements.reversed() {
            if value == parked {
                numberOfValues += 1
            } else {
                break
            }
        }
        
        print(latestMeasurements)
        print("Value: \(numberOfValues)")
        
        if numberOfValues >= minimumOfSameStateInARow {
            LEDFlasher.shared.flashLEDs()
            
            return
        }
        
        LEDFlasher.shared.setLEDValue(numberOfValues)
    }
}

extension Array where Element : Equatable {
    func allEqual() -> Bool {
        if let firstElem = first {
            return !dropFirst().contains { $0 != firstElem }
        }
        return true
    }
}
