//
//  Engine.swift
//  
//
//  Created by Pontus Jacobsson on 2021-03-04.
//

import Foundation
import SwiftyGPIO

public class DistanceSensor {
    enum ResultType {
        case distance(_ distance: Double)
        case error(_ error: Error)
    }
    
    static var hcsrSensor = HCSR04(usedRaspberry: .RaspberryPi3, echoConnectedPin: .P21, triggerConnectedPin: .P20, maximumSensorRange: 450)

    init() {}
    
    static func measureDistance(completion: @escaping (_ resultType: ResultType) -> ()) {
        do {
            let distance = try hcsrSensor.measureDistance()
            
            completion(.distance(distance))
        } catch {
            completion(.error(error))
        }
    }
}
