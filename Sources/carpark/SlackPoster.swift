//
//  SlackPoster.swift
//  
//
//  Created by Pontus Jacobsson on 2021-03-04.
//

import Foundation
#if canImport(FoundationNetworking)
import FoundationNetworking
#endif

public class SlackPoster {
    enum ParkingCase {
        case parkingSpotEmpty
        case parkingSpotUsed
    }
    
    static let webhookURL = URL(string: "https://hooks.slack.com/services/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")!
    
    static func postToSlack(_ parkingCase: ParkingCase) {
        var urlRequest = URLRequest(url: webhookURL)
        urlRequest.httpMethod = "POST"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-type")
        
        var messageText = ""
        
        switch parkingCase {
        case .parkingSpotEmpty:
            messageText = "*Parkeringen är nu ledig*\nNågon bil har lämnat parkeringen och det är fritt för andra att parkera 🏎💨"
        case .parkingSpotUsed:
            messageText = "*Parkeringen är nu Upptagen*\nNågon bil har tagit parkeringen 🏎🏠"
        }
        
        let body = "{\"text\": \"\(messageText)\"}"
        
        urlRequest.httpBody = body.data(using: .utf8)
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if error != nil {
                print("ERROR: \(error!)")
                
                return
            }
            
            if let data = data {
                print(String(data: data, encoding: .utf8) ?? "")
            }
            print(response ?? "")
        }.resume()
    }
    
}
