//
//  LEDFlasher.swift
//  
//
//  Created by Pontus Jacobsson on 2021-03-04.
//

import Foundation
import SwiftyGPIO

class LEDFlasher {
    static let shared = LEDFlasher()
    
    var gpios = SwiftyGPIO.GPIOs(for: .RaspberryPi3)
    var led1: GPIO!
    var led2: GPIO!
    var led3: GPIO!
    var led4: GPIO!
    var led5: GPIO!
    
    init() {
        setupLEDs()
    }
    
    private func setupLEDs() {
        led1 = gpios[.P4]
        led2 = gpios[.P24]
        led3 = gpios[.P23]
        led4 = gpios[.P22]
        led5 = gpios[.P27]
        
        led1.direction = .OUT
        led2.direction = .OUT
        led3.direction = .OUT
        led4.direction = .OUT
        led5.direction = .OUT
    }
    
    public func setLEDValue(_ value: Int) {
        led1.value = 0
        led2.value = 0
        led3.value = 0
        led4.value = 0
        led5.value = 0
        
        if value >= 1 {
            led1.value = 1
        }
        if value >= 2 {
            led2.value = 1
        }
        if value >= 3 {
            led3.value = 1
        }
        if value >= 4 {
            led4.value = 1
        }
        if value >= 5 {
            led5.value = 1
        }
    }
    
    public func flashLEDs() {
        
        led1.value = 1
        led2.value = 1
        led3.value = 1
        led4.value = 1
        led5.value = 1
        
        Thread.sleep(forTimeInterval: 0.25)
        
        led1.value = 0
        led2.value = 0
        led3.value = 0
        led4.value = 0
        led5.value = 0
        
        Thread.sleep(forTimeInterval: 0.25)
        
        led1.value = 1
        led2.value = 1
        led3.value = 1
        led4.value = 1
        led5.value = 1
        
        Thread.sleep(forTimeInterval: 0.25)
        
        led1.value = 0
        led2.value = 0
        led3.value = 0
        led4.value = 0
        led5.value = 0
        
        Thread.sleep(forTimeInterval: 0.25)
        
        led1.value = 1
        led2.value = 1
        led3.value = 1
        led4.value = 1
        led5.value = 1
    }
    
}
